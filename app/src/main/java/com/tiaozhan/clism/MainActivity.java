package com.tiaozhan.clism;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import Decoder.BASE64Encoder;
import io.reactivex.functions.Consumer;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback {
    /**
     * 主要代码来自
     * https://blog.csdn.net/maosidiaoxian/article/details/50774501
     */

    /**
     * 预览SurfaceView
     */
    public SurfaceView mSurfaceView;
    /**
     * 预览SurfaceView是否准备OK
     */
    public boolean isSurfaceViewReady;
    /**
     * 是否正在拍照片
     */
    public boolean mWaitForTakePhoto;
    /**
     * 相机引用
     */
    public Camera mCamera;
    /**
     * 后端可接受的最大边界尺寸
     */
    public static final int MAX_LENGTH = 700;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 全屏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams. FLAG_FULLSCREEN ,
                WindowManager.LayoutParams. FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        // 添加状态监听回调
        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceViewMain);
        SurfaceHolder holder = mSurfaceView.getHolder();
        holder.addCallback(this);
        // 设置宽高
        ViewGroup.LayoutParams params = mSurfaceView.getLayoutParams();
        params.height = 1920;
        params.width = 1080;
        mSurfaceView.setLayoutParams(params);
        // 点击屏幕时自动对焦
        mSurfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestFocus();
            }
        });

        View btp =  findViewById(R.id.imageViewhoto);
        btp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhoto();
            }
        });

        View bte = findViewById(R.id.imageViewExit);
        bte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        View btg = findViewById(R.id.imageViewGallery);
        btg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent t = new Intent(MainActivity.this, ShowActivity.class);
                t.putExtra("action", "gallery");
                startActivity(t);
            }
        });
    }

    /**
     * 弹框或者弹Toast给用户提示
     * @param msg 提示的文字
     */
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 将错误消息发给主线程显示
     * @param err
     */
    public void showErr(final String err) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showMessage(err);
            }
        });
    }

    /**
     * ============================================================
     * 相机权限部分
     * ============================================================
     */

    /**
     * 是否是高版本
     * @return 是或者不是
     */
    public boolean isHighVersion() {
        return Build.VERSION.SDK_INT >= 23;
    }

    /**
     * 检查有没有相机权限
     * @return 有或者没有
     */
    public boolean hasCameraPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            return ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    /**
     * 请求相机权限，低版本就直接完成
     */
    public void requestCameraPermission(final Runnable callback) {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions
                .request(Manifest.permission.CAMERA)
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean granted) {
                        if (granted) { // Always true pre-M
                            callback.run();
                        } else {
                            finish();
                        }
                    }
                });
    }

    /**
     * ============================================================
     * 相机预览部分
     * ============================================================
     */

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        isSurfaceViewReady = true;
        startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isSurfaceViewReady = false;
    }

    /**
     * 打开相机
     * 打开失败直接结束
     */
    public void openCamera() {
        if (mCamera == null) {
            try {
                mCamera = Camera.open();
            } catch (RuntimeException e) {
                if ("Fail to connect to camera service".equals(e.getMessage())) {
                    showMessage("无法打开相机，请检查是否已经开启权限");
                } else if ("Camera initialization failed".equals(e.getMessage())) {
                    showMessage("相机初始化失败，无法打开");
                } else {
                    showMessage("相机发生未知错误，无法打开");
                }
                finish();
            }
        } else {
            Log.i("clism", "相机已经打开，发现重复打开");
        }
    }

    /**
     * 关闭相机
     */
    private void closeCamera() {
        if (mCamera == null) {
            return;
        }
        mCamera.cancelAutoFocus();
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    /**
     * 设置相机参数
     */
    public void setParams() {
        Camera.Parameters mCameraParams;
        Camera.Size mBestPictureSize;
        Camera.Size mBestPreviewSize;
        // 获取相机参数
        mCameraParams = mCamera.getParameters();
        // 设置类型，旋转角度等
        mCameraParams.setPictureFormat(ImageFormat.JPEG);
        mCameraParams.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        mCameraParams.setRotation(90);
        // 获取宽高比
        float ratio = (float) mSurfaceView.getWidth() / mSurfaceView.getHeight();
        // 设置pictureSize
        List<Camera.Size> pictureSizes = mCameraParams.getSupportedPictureSizes();
        // 获取最佳尺寸
        mBestPictureSize = findBestPictureSize(pictureSizes, mCameraParams.getPictureSize(), ratio);
        mCameraParams.setPictureSize(mBestPictureSize.width, mBestPictureSize.height);

        // 将参数设置到相机
        mCamera.setParameters(mCameraParams);
    }

    /**
     * 初始化相机过程
     */
    public final Runnable initCamera = new Runnable() {
        @Override
        public void run() { initCameraImpl(); }
    };
    private void initCameraImpl() {
        openCamera();
        setParams();
        if (isSurfaceViewReady) {
            startPreview();
        } else {
        }
    }

    /**
     * 进入Activity时
     */
    @Override
    protected void onResume() {
        super.onResume();
        // 在SurfaceView子线程启动相机，我也不知道为啥要在子线程启动，可能是怕卡
        if (isHighVersion() && !hasCameraPermission()) {
            // 没有权限则申请权限
            // 通过权限后在子线程启动相机
            // 不通过则直接退出
            requestCameraPermission(new Runnable() {
                @Override
                public void run() {
                    mSurfaceView.post(initCamera);
                }
            });
        } else {
            // 有权限直接打开相机
            mSurfaceView.post(initCamera);
        }
    }

    /**
     * 退出页面时关闭相机
     */
    @Override
    protected void onPause() {
        super.onPause();
        closeCamera();
    }

    /**
     * 启动预览，令界面显示相机内容
     */
    private void startPreview() {
        if (mCamera == null) {
            return;
        }
        try {
            mCamera.setPreviewDisplay(mSurfaceView.getHolder());
            mCamera.setDisplayOrientation(90);
            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
            showMessage(e.getMessage()); // 显示错误信息
        }
    }

    /**
     * ============================================================
     * 相机尺寸匹配部分
     * ============================================================
     */

    /**
     * 尺寸数组排序，高宽比从大到小排列，注意是高宽比。
     * @param sizes 尺寸列表
     */
    public static void sortSizes(List<Camera.Size> sizes) {
        Collections.sort(sizes, new Comparator<Camera.Size>() {
            @Override
            public int compare(Camera.Size o1, Camera.Size o2) {
                return (o1.height * o2.width) - (o2.height * o1.width);
            }
        });
    }

    // 复制自csdn
    /**
     * 找到短边比长边大于于所接受的最小比例的最大尺寸
     *
     * @param sizes       支持的尺寸列表
     * @param defaultSize 默认大小
     * @param minRatio    相机图片短边比长边所接受的最小比例
     * @return 返回计算之后的尺寸
     */
    private Camera.Size findBestPictureSize(List<Camera.Size> sizes, Camera.Size defaultSize, float minRatio) {
        final int MIN_PIXELS = 320 * 480;

        sortSizes(sizes);

        Iterator<Camera.Size> it = sizes.iterator();
        while (it.hasNext()) {
            Camera.Size size = it.next();
            //移除不满足比例的尺寸
            if ((float) size.height / size.width <= minRatio) {
                it.remove();
                continue;
            }
            //移除太小的尺寸
            if (size.width * size.height < MIN_PIXELS) {
                it.remove();
            }
        }

        // 返回符合条件中最大尺寸的一个
        if (!sizes.isEmpty()) {
            return sizes.get(0);
        }
        // 没得选，默认吧
        return defaultSize;
    }

    /**
     * 根据图片尺寸，以及SurfaceView的比例来计算preview的尺寸。
     * @param sizes
     * @param defaultSize
     * @param pictureSize 图片的大小
     * @param minRatio preview短边比长边所接受的最小比例
     * @return
     */
    private Camera.Size findBestPreviewSize(List<Camera.Size> sizes, Camera.Size defaultSize,
                                            Camera.Size pictureSize, float minRatio) {
        final int pictureWidth = pictureSize.width;
        final int pictureHeight = pictureSize.height;
        boolean isBestSize = (pictureHeight / (float)pictureWidth) > minRatio;
        sortSizes(sizes);

        Iterator<Camera.Size> it = sizes.iterator();
        while (it.hasNext()) {
            Camera.Size size = it.next();
            if ((float) size.height / size.width <= minRatio) {
                it.remove();
                continue;
            }

            // 找到同样的比例，直接返回
            if (isBestSize && size.width * pictureHeight == size.height * pictureWidth) {
                return size;
            }
        }

        // 未找到同样的比例的，返回尺寸最大的
        if (!sizes.isEmpty()) {
            return sizes.get(0);
        }

        // 没得选，默认吧
        return defaultSize;
    }

    /**
     * ============================================================
     * 自动对焦以及拍照部分
     * ============================================================
     */

    /**
     * 请求自动对焦
     */
    public void requestFocus() {
        if (mCamera == null || mWaitForTakePhoto) {
            return;
        }
        mCamera.autoFocus(null);
    }

    /**
     * 拍摄照片
     */
    private void takePhoto() {
        if (mCamera == null || mWaitForTakePhoto) {
            return;
        }
        mWaitForTakePhoto = true;
        mCamera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                onTakePhoto(data);
                mWaitForTakePhoto = false;
                camera.startPreview();
            }
        });
    }

    /**
     * 将照片保存到文件中
     * @param data
     */
    private void onTakePhoto(final byte[] data) {
        new Thread(){
            @Override
            public void run() {
                byte[] resizedData = ImageUtil.resize4bytes(data, MAX_LENGTH);
                String image = ImageUtil.bytes2Base64(resizedData);
                Intent intent = new Intent(MainActivity.this, ShowActivity.class);
                intent.putExtra("imageData", image);
                startActivity(intent);
            }
        }.start();
    }


}
