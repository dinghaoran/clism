package com.tiaozhan.clism;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.Random;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 全屏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams. FLAG_FULLSCREEN ,
                WindowManager.LayoutParams. FLAG_FULLSCREEN);
        // TODO 随机换一个屏幕
        int x = (new Random()).nextInt(3);
        if (x == 0) {
            setContentView(R.layout.activity_welcome1);
        } else if (x == 1) {
            setContentView(R.layout.activity_welcome2);
        } else {
            setContentView(R.layout.activity_welcome3);
        }

        View btp = findViewById(R.id.imageViewPhoto);
        btp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
            }
        });
    }
}
