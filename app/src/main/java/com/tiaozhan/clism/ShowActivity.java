package com.tiaozhan.clism;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import Decoder.BASE64Encoder;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ShowActivity extends AppCompatActivity {
    /**
     * 用于显示图片的ImageView
     */
    public ImageView imageViewShow;
    /**
     * 唯一的请求URL
     */
    public final static String URL = "https://clism.ddltech.top/api/image";
    /**
     * OKHttp客户端
     */
    public OkHttpClient mClient;
    /**
     * 经过压缩的base64字符串图像数据
     */
    public String imageData;
    /**
     * 上次发送的风格化请求的code
     */
//    public String code;
    /**
     * 风格化结果字符串
     */
    public Bitmap result;
    /**
     * 进度条对话框
     */
    public ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        Intent t = getIntent();
        Bundle ext = t.getExtras();

        boolean flag = false;
        if (ext == null) {
            showErr("相机无数据");
            finish();
        } else {
            imageData = ext.getString("imageData");
            if (imageData == null || imageData.isEmpty()) {
                String action = ext.getString("action");
                if ("gallery".equals(action)) {
                    openGallery();
                } else {
                    showErr("相机无数据");
                    finish();
                }
                flag = false;
            } else {
                flag = true;
            }

        }

        this.imageViewShow = (ImageView) findViewById(R.id.imageViewShow);
        if (flag) {
            imageViewShow.setImageBitmap(ImageUtil.bytes2Bitmap(ImageUtil.base642bytes(imageData)));
        }

        mClient = new OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build();

        createDialog();
    }

    /**
     * 弹框或者弹Toast给用户提示
     * @param msg 提示的文字
     */
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 将错误消息发给主线程显示
     * @param err
     */
    public void showErr(final String err) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showMessage(err);
                if (pDialog.isShowing()) {
                    closeDialog();
                }
            }
        });
    }

    /**
     * ============================================================
     * 原生初始化部分
     * ============================================================
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_photo) {
//            takePhoto();
//            return true;
//        } else if (id == R.id.action_post) {
//            // 上传照片
//        } else if (id == R.id.action_get) {
//            new Thread() {
//                @Override
//                public void run() {
//                    getImage();
//                }
//            }.start();
//        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * ============================================================
     * 前后端请求部分
     * ============================================================
     */

    public static final MediaType TYPE_JSON
            = MediaType.parse("application/json; charset=utf-8");

    /**
     * 向后端的post请求是否结束
     */
    public boolean isAjaxFinished = true;

    /**
     * 上传处理前图片
     * @param data
     */
    private void postImage(String style) {
        isAjaxFinished = false;
        String url = URL;
        String image = "data:image/jpg;base64," + imageData;
        FormBody.Builder paramsBuilder = new FormBody.Builder();
        paramsBuilder.add("image", image);
        paramsBuilder.add("style", style);
        Request request = new Request.Builder().url(url).post(paramsBuilder.build()).build();
        try {
            startLoop();
            Response response = mClient.newCall(request).execute();
            String res = response.body().string();
            JsonParser parser = new JsonParser();
            JsonObject jsonRes = (JsonObject) parser.parse(res);
            int success =  jsonRes.get("success").getAsInt();
            if (success == 1) {
                JsonObject jsonData = jsonRes.get("data").getAsJsonObject();
                String backImage = jsonData.get("image").getAsString();
                byte[] imageData = ImageUtil.base642bytes(backImage);
                this.result = ImageUtil.bytes2Bitmap(imageData);
                this.isAjaxFinished = true;
            } else {
                String err = jsonRes.get("err_msg").getAsString();
                showErr(err);
                this.isAjaxFinished = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            showErr("网络有点慢，再试一次吧！" + e.toString());
            this.isAjaxFinished = true;
        }
    }

    /**
     * 开始循环检查结果
     */
    private void startLoop() {
        new Thread() {
            long last;
            @Override
            public void run() {
                last = System.currentTimeMillis();
                try {
                    for (int i = 0; i < 60; i++) {
                        nextTick(1000);
                        String status = "";
                        if (i < 10) {
                            status = "正在观察图像";
                        } else if (i < 20) {
                            status = "正在学习风格";
                        } else if (i < 30) {
                            status = "正在临摹边缘";
                        } else if (i < 40) {
                            status = "正在绘制主体";
                        } else if (i < 50) {
                            status = "正在描摹细节";
                        } else {
                            status = "正在完成画作";
                        }
                        setProgress(i, status);
                        if (isAjaxFinished) {
                            showResult();
                            showErr("画好了");
                            closeDialog();
//                            code = null;
                            return;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                showErr("画作突然爆炸了");
                closeDialog();
//                code = null;
            }

            /**
             * 等待下一个毫秒倍数
             * @param millis
             */
            private void nextTick(long millis) {
                while (System.currentTimeMillis() - last < millis) {
                    try {
                        sleep(10);
                    } catch (InterruptedException e) {
                    }
                }
                last = System.currentTimeMillis();
            }
        }.start();
    }

    /**
     * 当用户点击了某个样式时
     * @param v
     */
    public void chooseStyle(View v) {
        final String style = (String) v.getTag();
        if (!isAjaxFinished) {
            showErr("请等待上一个任务完成");
            return;
        }
        if (style.equals("0")) {
            byte[] imageData = ImageUtil.base642bytes(this.imageData);
            this.result = ImageUtil.bytes2Bitmap(imageData);
            showResult();
        } else {
            new Thread(){
                @Override
                public void run() {
                    postImage(style);
                }
            }.start();
            pDialog.show();
        }
    }

    /**
     * 在主线程显示
     */
    private void showResult() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageViewShow.setImageBitmap(result);
            }
        });
    }

    /**
     * 创建进度条对话框
     */
    public void createDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setIcon(R.drawable.logo);
        pDialog.setTitle("正在迁移画风");
        pDialog.setMessage("正在上传文件");
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setMax(60);
        pDialog.setProgress(0);
        pDialog.setCancelable(false);
    }

    /**
     * 设置当前进度条进度
     * @param progress
     * @param status
     */
    public void setProgress(final int progress, final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.setProgress(progress);
                pDialog.setMessage(status);
            }
        });
    }

    /**
     * 关闭并重置Dialog
     */
    public void closeDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.dismiss();
                createDialog();
            }
        });
    }

    /**
     * ============================
     * 图库
     * ============================
     */

    public static final int REQUEST_PICTURE = 66;

    /**
     * 打开图库，请求码为
     */
    public void openGallery() {
        String action;
        if (Build.VERSION.SDK_INT >= 19) { //19代表KITKAT
            action = Intent.ACTION_OPEN_DOCUMENT;
        } else {
            action = Intent.ACTION_GET_CONTENT;
        }

        Intent t = new Intent(action);
        t.setType("image/*");
        t.putExtra("crop", true);
        t.putExtra("return-data", true);
        startActivityForResult(t, REQUEST_PICTURE);
    }

    /**
     * 给定URL，读取为字节
     * @param path
     * @return
     * @throws FileNotFoundException
     */
    private byte[] uri2Bytes(@NonNull Uri path) throws FileNotFoundException {
        ContentResolver resolver = this.getContentResolver();
        InputStream imageIS = resolver.openInputStream(path);
        Bitmap source = BitmapFactory.decodeStream(imageIS);
        Bitmap bitmap = ImageUtil.resize(source, MainActivity.MAX_LENGTH);
        return ImageUtil.bitmap2Bytes(bitmap);
    }

    /**
     * 处理Uri对应的显示逻辑
     * @param data
     */
    public void onChoosedImage(Intent data) {
        final Uri path = data.getData();
        if (path == null) {
            showErr("图片地址有误 您选择图片了吗？");
            finish();
            return;
        }
        new Thread() {
            @Override
            public void run() {
                try {
                    byte[] imageDataBytes = uri2Bytes(path);
                    final Bitmap clone = ImageUtil.bytes2Bitmap(imageDataBytes);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imageViewShow.setImageBitmap(clone);
                        }
                    });
                    imageData = ImageUtil.bytes2Base64(imageDataBytes);
                } catch (Exception e) {
                    e.printStackTrace();
                    showErr("文件不存在");
                }
            }
        }.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case REQUEST_PICTURE:
                    onChoosedImage(data);
            }
        }
    }
}
