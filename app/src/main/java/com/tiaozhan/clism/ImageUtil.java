package com.tiaozhan.clism;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

/**
 * 图像工具
 * Created by FinnTenzor on 2018/6/2.
 */

public class ImageUtil {

    // 压缩用的图像格式
    public static final Bitmap.CompressFormat IMAGE_FORMAT = Bitmap.CompressFormat.JPEG;

    /**
     * 字节数组转bitmap
     * @param b
     * @return
     */
    public static final Bitmap bytes2Bitmap(byte[] b) {
        if (b.length != 0) {
            return BitmapFactory.decodeByteArray(b, 0, b.length);
        } else {
            return null;
        }
    }

    /**
     * 图像重新设定大小
     * @param source 原图像
     * @param max 最大尺寸
     * @return
     */
    public static final Bitmap resize(Bitmap source, int max) {
        int sw = source.getWidth();
        int sh = source.getHeight();
        int w = sw;
        int h = sh;
        if (sw < max && sh < max) {
            return source;
        }
        if (sw > sh) {
            w = max;
            h = (int)(sh * ((double)max / sw));
        } else {
            w = (int)(sw * ((double)max / sh));
            h = max;
        }
        return resize(source, w, h);
    }

    /**
     * 调整bitmap大小
     * @param source
     * @param w
     * @param h
     * @return
     */
    public static final Bitmap resize(Bitmap source, int w, int h) {
        return ThumbnailUtils.extractThumbnail(source, w, h);
    }

    /**
     * bitmap转字节
     * @param source
     * @return
     */
    public static final byte[] bitmap2Bytes(Bitmap source) {
        ByteArrayOutputStream baos =  new ByteArrayOutputStream();
        source.compress(IMAGE_FORMAT, 70, baos);
        source.recycle();
        return baos.toByteArray();
    }

    /**
     * bitmap转字节
     * @param source
     * @param quality
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static final byte[] bitmap2Bytes(Bitmap source, int quality) {
        ByteArrayOutputStream baos =  new ByteArrayOutputStream();
        source.compress(IMAGE_FORMAT, quality, baos);
        source.recycle();
        return baos.toByteArray();
    }

    /**
     * 将字节数组保存的图像数据缩放
     * @param data
     * @param max
     * @return
     */
    public static final byte[] resize4bytes(byte[] data, int max) {
        Bitmap source = bytes2Bitmap(data);
        Bitmap resized = resize(source, max);
        return bitmap2Bytes(resized);
    }

    public static final String bytes2Base64(byte[] image) {
        return new BASE64Encoder().encode(image);
    }

    public static final byte[] base642bytes(String base64) {
        try {
            return new BASE64Decoder().decodeBuffer(base64);
        } catch (IOException e) {
            return null;
        }
    }
}
